terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.0.0"
    }
  }
}

module "lambda" {
  source = "./modules/lambda"

  lambda_name      = "gruntwork-lambda-tutorial"
  handler          = "main.lambda_handler"
  source_file      = "${path.module}/main.py"
  runtime          = "python3.9"
}


output "function_name" {
  value = module.lambda.function_name
}