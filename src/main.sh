#!/bin/bash
export FUNCTION_NAME=$(terraform output -raw function_name)
aws lambda invoke --function-name $FUNCTION_NAME --output json lambda_output
