import uuid
import json
from urllib.request import urlopen, Request


def lambda_handler(event, context):
    url = "https://t.gruntwork.io/"

    github_username = "ProjectGreenfield"

    # This code sends an event to Gruntwork that includes your GitHub username to
    # signify that you completed the tutorial.
    # We don't track anything else about you other than your GitHub username, and we
    # only use this data internally to understand who has completed our tutorial

    payload = {
        "id": uuid.uuid4().hex,
        "event": "ModuleTutorialDeploymentComplete",
        "eventProps": {
            "distinct_id": github_username,
            "github_username": github_username,
            "$insert_id": uuid.uuid4().hex
        }
    }

    headers = {
      "accept": "text/plain",
      "content-type": "application/json"
    }

    httprequest = Request(url, headers=headers, method="POST",
    data=json.dumps(payload).encode('utf-8'))

    response_object = {}

    with urlopen(httprequest) as response:
        response_object["operation_status"] = "Success"
        response_object["statusCode"] = response.status
        response_object["body"] = f"Hello, {github_username}, from Gruntwork!"

    return response_object
    