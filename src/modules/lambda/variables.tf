variable "lambda_name" {
  type        = string
  description = "Name that will be used for the AWS Lambda function"
}

variable "handler" {
  type        = string
  description = "The name of the handler function that will be called as the entrypoint of the lambda"
}

variable "source_file" {
  type        = string
  description = "The path to the source file to be deployed to lambda"
}

variable "runtime" {
  type        = string
  description = "The runtime of the Lambda. Options include go, python, ruby, etc."
}

variable "memory_size" {
  type        = number
  description = "The amount of memory, in MB, to give to the Lambda. Defaults to 128."
  default     = 128
}

variable "timeout" {
  type        = number
  description = "The amount of time, in seconds, that a lambda can execute before timing out. Defaults to 30."
  default     = 30
}