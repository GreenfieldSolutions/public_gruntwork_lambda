# Purpose
Build an AWS lambda function that responds back with Hello "YOUR USERNAME". Using Terraform to automate the deployment.

# Software Version 
* Terraform v1.7.3
* Terragrunt v0.55.2
* Python v3.10.12

# Resources
## Setup for Gruntwork
* [Setting up your machine](https://docs.gruntwork.io/library/getting-started/setting-up)
  * Terraform and Terragrunt (optional)
## Terraform
* [Installation](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli#install-cli)
* [Terraform Language Documentation](https://developer.hashicorp.com/terraform/language)
* [Provisioning](https://developer.hashicorp.com/terraform/cli/run)
## Terragrunt
* [Installation](https://terragrunt.gruntwork.io/docs/getting-started/install/) 
* [About Terragrunt](https://terragrunt.gruntwork.io/)